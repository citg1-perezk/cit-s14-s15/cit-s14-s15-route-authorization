//Classes where services are mapped to their corresponding endpoints and HTTP methods via @RequestMapping annotation.
package com.zuitt.discussion.controllers;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.UserRepository;
import com.zuitt.discussion.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
//enables cross origin request via @CrossOrigin
@CrossOrigin
public class PostController {
    @Autowired
    PostService postService;
    //Create Post


    @RequestMapping(value="/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){


          postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully.", HttpStatus.CREATED);
    //Get All Post
    }
    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPosts(){
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }
    // Delete Post
    @RequestMapping(value ="/posts/{postid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePosts(@PathVariable Long postid){
        return new ResponseEntity<>(postService.deletePost(postid), HttpStatus.OK);
    }
    //Update Post
    @RequestMapping(value = "/posts/{postid}", method=RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long postid, @RequestBody Post post){
        return postService.updatePost(postid, post);
    }

}
