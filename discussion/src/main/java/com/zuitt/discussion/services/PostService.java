//Service is an interface that exposes the methods of an implementation whose details have been abstracted away.

package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import org.springframework.http.ResponseEntity;

import java.util.Iterator;

public interface PostService {
    //Create a post
    void createPost(String stringToken, Post post);

    //Viewing all posts
    Iterable<Post> getPosts();
    //Delete a posts
    ResponseEntity deletePost(Long id);
    //Update posts
    ResponseEntity updatePost(Long id, Post post);
}
